# Login App

This application can do registration, OTP Verification, login and logout process. And then show user profile when registration successful. Profile data, photo profile, and cover can be updated by the user.

## Preview
![page1](./preview1.png)
![page2](./preview2.png)
![page3](./preview3.png)
![page4](./preview4.png)
![page5](./preview5.png)

## Build Setup

``` bash
# install Node Module dependencies
npm install

# runs the app in the development mode.
npm start

# build for production with minification
npm build
```

## Technologies
This project uses several technologies listed below :

- **[React Router](https://reactrouter.com/)**, is a collection of navigational components that compose declaratively with your application
- **[Sass](https://sass-lang.com/)**, is a preprocessor scripting language that is interpreted or compiled into Cascading Style Sheets.
- **[React Bootstrap](https://react-bootstrap.github.io/)**, is Bootstrap components built with React.
- **[Axios](https://github.com/axios/axios)** for API Consuming.

