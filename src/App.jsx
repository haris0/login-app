import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import LoginPage from './components/LoginPage/LoginPage'
import RegisterPage from './components/Register/RegisterPage'
import OTPPage from './components/OTPPage/OTPPage'
import ProfilePage from './components/Profile/ProfilePage'
import Page404 from './components/Page404/Page404'
import EditProfile from './components/EditProfile/EditProfile'

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Redirect exact path="/" to="/login"/>
          <Route exact path="/login" component={LoginPage}/>
          <Route exact path="/register" component={RegisterPage}/>
          <Route exact path="/otp/:phone/:id" children={<OTPPage/>}/>
          <Route exact path="/profile" component={ProfilePage}/>
          <Route exact path="/edit-profile" component={EditProfile}/>
          <Route exact path="/404" component={Page404}/>
          <Redirect path="*" to="/404"/>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
