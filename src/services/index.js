import axios from "axios";

export const register = 'register'
export const requestOtp = 'register/otp/request'
export const matchOTP =  'register/otp/match'
export const revokeOauth = 'oauth/revoke'
export const loginOauth = 'oauth/sign_in'
export const profile = 'profile/me'
export const updateProfile = 'profile'
export const updateJob = 'profile/career'
export const updateSchool = 'profile/education'
export const uploadCover = 'uploads/cover'
export const uploadPhotos = 'uploads/profile'

export const HTTP = axios.create({
  baseURL: 'http://pretest-qa.dcidev.id/api/v1/',
  timeout: 3000,
  headers: {
    'Content-Type': 'multipart/form-data',
  }
});