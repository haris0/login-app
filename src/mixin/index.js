import {HTTP, profile} from '../services'

const getPosition =()=> {
  return new Promise((res, rej) => {
      navigator.geolocation.getCurrentPosition(res, rej);
  });
}

export const getLatlong =async()=>{
  let lat = ""
  let long = ""
  try {
    let pos = await getPosition()
    let coords = pos.coords
    lat = coords.latitude
    long = coords.longitude
  } catch (error) {
    lat = ""
    long = ""
  }
  return [lat, long].join(",")
}

export const getDeviceType =()=>{
  let userAgent = navigator.userAgent || navigator.vendor || window.opera;
  let device = 2
  if(/android/i.test(userAgent)) {
    device = 1
  }
  if(/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
    device = 0
  }
  return device
}

export const getProfile =async()=>{
  try {
    let response = await HTTP.get(profile,{
      params :{
        access_token:localStorage.getItem('token'),
      }
    })
    return response
  } catch (error) {
    return error.response
  }
}

export const getYears =(date)=>{
  return date.split("-")[0]
}