import React, {useState, useRef} from 'react'
import './OTPPage.scss'
import {Container,
        Card,
        Form,
        Col,
        Button,
        Alert} from 'react-bootstrap'
import {useHistory, useParams} from 'react-router-dom'
import {HTTP, requestOtp, matchOTP} from '../../services'

export default function OTPPage() {

  let history = useHistory()
  const { phone, id } = useParams()

  const [opt1, setOpt1] = useState('');
  const opt1Ref = useRef();
  const [opt2, setOpt2] = useState('');
  const opt2Ref = useRef();
  const [opt3, setOpt3] = useState('');
  const opt3Ref = useRef();
  const [opt4, setOpt4] = useState('');
  const opt4Ref = useRef();
  const buttonRef = useRef();

  const [errorOtp, setErrorOtp] = useState({status:false, message:""})

  const [resendLoad, setResendLoad] = useState(false)
  const [verifLoad, setVerifLoad] = useState(false)

  const handleChange = (e, setOpt, optRef) => {
    setOpt(e.target.value)
    if (e.target.value.length === 1) {
      optRef.current.focus();
    }
  }

  const handleClick =()=>{
    setErrorOtp({status:false, message:""})
  }

  const handleVerif =()=>{
    if(opt1 === "" || opt2 === "" || opt3 === "" || opt4 === ""){
      setErrorOtp({status:true, message:"OTP is required"})
    }else{
      let verifCode = [opt1, opt2, opt3, opt4].join("")
      console.log(verifCode)
      matchingOTP(verifCode)
    }
  }

  const matchingOTP = async(code)=>{
    setVerifLoad(true)
    let formData = new FormData()
    formData.append("user_id", id);
    formData.append("otp_code", code);
    try {
      let response = await HTTP.post(matchOTP, formData)
      console.log(response)
      let token = response.data.data.user.access_token
      localStorage.setItem("token", token);
      history.push('/profile')
    } catch (error) {
      console.log(error.response)
    }
    setVerifLoad(false)
    // Pada API otp/match code yang di masukkan selalu berhasil
    // Sehingga saya tidak buat handle error karean tidak tahu respons error yang akan diberikan
  }

  const resendOTP =async()=>{
    setResendLoad(true)
    let formData = new FormData()
    formData.append("phone", phone);
    console.log(phone)
    try {
      let response = await HTTP.post(requestOtp, formData)
      console.log(response)
    } catch (error) {
      console.log(error.response)
    }
    setResendLoad(false)
  }
  
  return (
    <div className="otp">
      <Container>
      <div className="center-posistion">
        <Alert variant='dark' className='guide'>
          Code Verifikasi telah di kirim ke {phone}, masukkan kode verifikasi
        </Alert>
        <Card body>
          <div className="title">
            Verifikasi OTP
          </div>
          <Form>
            <Form.Row>
              <Col>
                <Form.Control 
                  type="number"
                  className='otp-input'
                  ref={opt1Ref}
                  onChange={ (e) => { handleChange(e, setOpt1, opt2Ref) }}
                  onClick={handleClick}
                  autoComplete="off"
                  maxLength="1"
                  placeholder="0" />
              </Col>
              <Col>
                <Form.Control 
                  type="number"
                  className='otp-input'
                  ref={opt2Ref}
                  onChange={ (e) => { handleChange(e, setOpt2, opt3Ref) }}
                  onClick={handleClick}
                  autoComplete="off"
                  maxLength="1"
                  placeholder="0" />
              </Col>
              <Col>
                <Form.Control 
                  type="number"
                  className='otp-input'
                  ref={opt3Ref}
                  onChange={ (e) => { handleChange(e, setOpt3, opt4Ref) }}
                  onClick={handleClick}
                  autoComplete="off"
                  maxLength="1"
                  placeholder="0" />
              </Col>
              <Col>
                <Form.Control 
                  type="number"
                  className='otp-input'
                  ref={opt4Ref}
                  onChange={ (e) => { handleChange(e, setOpt4, buttonRef) }}
                  onClick={handleClick}
                  autoComplete="off"
                  maxLength="1"
                  placeholder="0" />
              </Col>
            </Form.Row>
            {errorOtp.status && 
              <Form.Text className="text-muted error-text">
                {errorOtp.message}
              </Form.Text>
            }
            <Button 
              ref={buttonRef} 
              variant="primary" 
              onClick={handleVerif}
              className='verif-button'>
              {verifLoad ? "verifying...": "Verifikasi"}
            </Button>
          </Form>
        </Card>
        <Card body className='resend-card'>
          <span className='resend-text' onClick={resendOTP}>Resend OTP Code! {resendLoad && "Seding Code..."}</span>
        </Card>
      </div>
      </Container>
    </div>
  )
}
