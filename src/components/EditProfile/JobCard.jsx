import React, {useState, useEffect} from 'react'
import {
  Card,
  Button,
  Form,
  Col,
  Alert} from 'react-bootstrap'
import {getProfile} from '../../mixin'
import {HTTP, updateJob} from '../../services'

export default function JobCard() {

  const [position, setPosition] = useState('')
  const [company, setCompany] = useState('')
  const [start, setStart] = useState('')
  const [end, setEnd] = useState('')
  const [success, setSuccess] = useState(false)

  const handlePosition = e => setPosition(e.target.value)
  const handleCompany = e => setCompany(e.target.value)
  const handleStart = e => setStart(e.target.value)
  const handleEnd = e => setEnd(e.target.value)

  const setJobField =(career)=>{
    if(career.position ) setPosition(career.position)
    if(career.company_name) setCompany(career.company_name)
    if(career.starting_from) setStart(career.starting_from)
    if(career.ending_in) setEnd(career.ending_in)
  }

  const handleUpdatJob =async()=>{
    let startDate = new Date(start)
    let endDate = new Date(end)
    let formData = new FormData()
    formData.append("position", position);
    formData.append("company_name", company);
    formData.append("starting_from", startDate);
    formData.append("ending_in", endDate);
    formData.append("access_token", localStorage.getItem('token'));
    try {
      let response = await HTTP.post(updateJob, formData)
      console.log(response)
      setSuccess(true)
    } catch (error) {
      console.log(error.response)
    }
  }

  useEffect(()=>{
    const loadProfile =async()=>{
      let profileData = await getProfile()
      if(profileData.status){
        if(profileData.status === 200){
          setJobField(profileData.data.data.user.career)
        }
      }
    }
    loadProfile()
  },[])
  return (
    <div>
      <Card>
        <Card.Body>
          <Card.Title>Update Last Job</Card.Title>
          <hr/>
          <Form.Group controlId="Position">
            <Form.Label>Position</Form.Label>
            <Form.Control 
              type="text" 
              placeholder="Position"
              value={position}
              onChange={handlePosition}/>
          </Form.Group>
          <Form.Group controlId="Company">
            <Form.Label>Company</Form.Label>
            <Form.Control 
              type="text" 
              placeholder="Company" 
              value={company}
              onChange={handleCompany}/>
          </Form.Group>
          <Form.Group controlId="Year">
            <Form.Row>
              <Col>
                <Form.Label>Start</Form.Label>
                <Form.Control 
                  type="date"
                  value={start}
                  onChange={handleStart}/>
              </Col>
              <Col>
                <Form.Label>End</Form.Label>
                <Form.Control 
                  type="date"
                  value={end}
                  onChange={handleEnd}/>
              </Col>
            </Form.Row>
          </Form.Group>
          <Button 
            variant="primary" 
            className='update-button'
            onClick={handleUpdatJob}>
            Update
          </Button>
          {success &&
            <Alert 
              style={{marginTop: "20px"}}  
              onClose={() => setSuccess(false)}  
              variant='success' 
              dismissible>
              Job Successfully Updated
            </Alert>
          }
        </Card.Body>
      </Card>
    </div>
  )
}
