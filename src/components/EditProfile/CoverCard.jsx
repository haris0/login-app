import React,{useState, useEffect} from 'react'
import {
  Card,
  Button,
  Form,
  Alert} from 'react-bootstrap'
import {getProfile} from '../../mixin'
import {HTTP, uploadCover} from '../../services'

export default function CoverCard() {

  const [cover, setCover] = useState('')
  const [success, setSuccess] = useState(false)
  const [emptyFile, setEmptyFile] = useState(false)
  const [coverUrl, setCoverUrl] = useState('https://via.placeholder.com/920x190.png?text=Cover+Unset')
  const [uploading, setUploading] = useState(false)

  const handleCover = e => setCover(e.target.files[0])

  const handleUploadCover =async()=>{
    if(cover === ''){
      setEmptyFile(true)
    }else{
      setUploading(true)
      let formData = new FormData()
      formData.append('image', cover)
      formData.append("access_token", localStorage.getItem('token'));
      try {
        let response = await HTTP.post(uploadCover, formData)
        console.log(response)
        let url = response.data.data.user_picture.cover_picture.url
        setCoverUrl(url)
        setSuccess(true)
      } catch (error) {
        console.log(error.response)
      }
      setUploading(false)
    }
  }

  useEffect(()=>{
    const loadProfile =async()=>{
      let profileData = await getProfile()
      if(profileData.status){
        if(profileData.status === 200){
          let url = profileData.data.data.user.cover_picture.url
          if(url) setCoverUrl(url)
        }
      }
    }
    loadProfile()
  },[])

  return (
    <div>
      <Card>
        <Card.Img variant="top" src={coverUrl} />
        <Card.Body>
          <Card.Title>Update Cover</Card.Title>
          <hr/>
          <Form.Group controlId="file-cover">
            <Form.File id="cover">
              <Form.File.Label>Choose Image</Form.File.Label>
              <Form.File.Input 
                accept="image/png, image/jpeg"
                onChange={handleCover}
                onClick={()=>(setEmptyFile(false))}/>
            </Form.File>
            {emptyFile &&
              <Form.Text style={{color:'red'}}>
                Select Image First
              </Form.Text>
            }
          </Form.Group>
          <Button 
            variant="primary"
            className='update-button'
            onClick={handleUploadCover}>
            {uploading ? "Uploading...":"Upload"}
          </Button>
          {success &&
            <Alert 
              style={{marginTop: "20px"}}  
              onClose={() => setSuccess(false)}  
              variant='success' 
              dismissible>
              Cover Successfully Upload
            </Alert>
          }
        </Card.Body>
      </Card>
    </div>
  )
}
