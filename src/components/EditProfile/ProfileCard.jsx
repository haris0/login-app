import React,{useState, useEffect} from 'react'
import {
  Card,
  Button,
  Form,
  Alert} from 'react-bootstrap'
import {getProfile} from '../../mixin'
import {HTTP, updateProfile} from '../../services'

const convertGender =(gender)=>{
  if(gender === 'male'){
    return 0
  }
  return 1
}

export default function ProfileCard() {

  const [name, setName] = useState('')
  const [gender, setGender] = useState('0')
  const [birthday, setBirthday] = useState('')
  const [hometown, setHometown] = useState('')
  const [bio, setBio] = useState('')
  const [success, setSuccess] = useState(false)

  const handleName = e => setName(e.target.value)
  const handleGender = e => setGender(e.target.value)
  const handleBirthday = e => setBirthday(e.target.value)
  const handleHometown = e => setHometown(e.target.value)
  const handleBio = e => setBio(e.target.value)

  const handleUpdate =async()=>{
    let birthdayDate = new Date(birthday)
    let formData = new FormData()
    console.log(gender+" "+typeof(gender))
    formData.append("name", name);
    formData.append("gender", gender);
    formData.append("birthday", birthdayDate);
    formData.append("hometown", hometown);
    formData.append("bio", bio);
    formData.append("access_token", localStorage.getItem('token'));
    try {
      let response = await HTTP.post(updateProfile, formData)
      console.log(response)
      setSuccess(true)
    } catch (error) {
      console.log(error.response)
    }
  }

  const setProfileField =(profile)=>{
    if(profile.name !== profile.id ) setName(profile.name)
    if(profile.gender) setGender(convertGender(profile.gender))
    if(profile.birthday) setBirthday(profile.birthday)
    if(profile.hometown) setHometown(profile.hometown)
    if(profile.bio) setBio(profile.bio)
  }

  useEffect(()=>{
    const loadProfile =async()=>{
      let profileData = await getProfile()
      if(profileData.status){
        if(profileData.status === 200){
          setProfileField(profileData.data.data.user)
        }
      }
    }
    loadProfile()
  },[])

  return (
    <div>
      <Card >
        <Card.Body>
          <Card.Title>Update Profile Data</Card.Title>
          <hr/>
          <Form>
            <Form.Group controlId="Name">
              <Form.Label>Name</Form.Label>
              <Form.Control 
                type="text" 
                placeholder="Name" 
                value={name}
                onChange={handleName}/>
            </Form.Group>
            <Form.Group controlId="SelectGender">
              <Form.Label>Gender</Form.Label>
              <Form.Control 
                as="select"
                value={gender}
                onChange={handleGender}>
                <option value='0'>Male</option>
                <option value='1'>Female</option>
              </Form.Control>
            </Form.Group>
            <Form.Group controlId="Birthday">
              <Form.Label>Birthday</Form.Label>
              <Form.Control 
                type='date'
                value={birthday}
                onChange={handleBirthday}/>
            </Form.Group>
            <Form.Group controlId="Hometown">
              <Form.Label>Hometown</Form.Label>
              <Form.Control 
                type="text" 
                placeholder="Hometown" 
                value={hometown}
                onChange={handleHometown}/>
            </Form.Group>
            <Form.Group controlId="Bio">
              <Form.Label>Bio</Form.Label>
              <Form.Control 
                as="textarea" 
                placeholder="Bio"
                value={bio}
                onChange={handleBio}/>
            </Form.Group>
            <Button 
              variant="primary" 
              className='update-button'
              onClick={handleUpdate}>
              Update
            </Button>
          </Form>
          {success &&
            <Alert 
              style={{marginTop: "20px"}}  
              onClose={() => setSuccess(false)}  
              variant='success' 
              dismissible>
              Profile Successfully Updated
            </Alert>
          }
        </Card.Body>
      </Card>
    </div>
  )
}
