import React from 'react'
import './EditProfile.scss'
import { Container } from 'react-bootstrap'
import CoverCard from './CoverCard'
import ProfileCard from './ProfileCard'
import PhotosCard from './PhotosCard'
import JobCard from './JobCard'
import EducationCard from './EducationCard'

export default function EditProfile() {
  return (
    <div className='edit'>
      <Container className='conatiner'>
        <div className='edit-card'>
          <CoverCard/>
        </div>
        <div className='edit-card'>
          <PhotosCard/>
        </div>
        <div className='edit-card'>
          <ProfileCard/>
        </div>
        <div className='edit-card'>
          <JobCard/>
        </div>
        <div className='edit-card'>
          <EducationCard/>
        </div>
      </Container>
    </div>
  )
}
