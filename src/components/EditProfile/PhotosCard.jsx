import React,{useState, useEffect} from 'react'
import {
  Card,
  Button,
  Form,
  Alert} from 'react-bootstrap'
import {getProfile} from '../../mixin'
import {HTTP, uploadPhotos} from '../../services'
import avatar from '../../assets/avatar.png'

export default function PhotosCard() {

  const [photos, setPhotos] = useState('')
  const [emptyFile, setEmptyFile] = useState(false)
  const [photosUrl, setPhotosUrl] = useState(avatar)
  const [success, setSuccess] = useState(false)
  const [uploading, setUploading] = useState(false)

  const handlePhotos = e => setPhotos(e.target.files[0])

  const handleUploadPhotos =async()=>{
    if(photos === ''){
      setEmptyFile(true)
    }else{
      setUploading(true)
      let formData = new FormData()
      formData.append("image", photos);
      formData.append("access_token", localStorage.getItem('token'));
      try {
        let response = await HTTP.post(uploadPhotos, formData)
        console.log(response)
        setSuccess(true)
        setPhotosUrl(response.data.data.user_picture.picture.url)
      } catch (error) {
        console.log(error.response)
      }
      setUploading(false)
    }
  }

  useEffect(()=>{
    const loadProfile =async()=>{
      let profileData = await getProfile()
      console.log(profileData.status)
      console.log(profileData)
      if(profileData.status){
        if(profileData.status === 200){
          let user_picture = profileData.data.data.user.user_picture
          if(user_picture) setPhotosUrl(user_picture.picture.url)
        }
      }
    }
    loadProfile()
  },[])

  return (
    <div>
      <Card>
        <Card.Body>
          <div className='center-avatar'>
            <img src={photosUrl} alt="avatar" className="avatar"/>
          </div>
          <Card.Title>Upload Photos</Card.Title>
          <hr/>
          <Form.Group controlId="file-photos">
            <Form.File id="photo">
              <Form.File.Label>Choose Images</Form.File.Label>
              <Form.File.Input
                accept="image/png, image/jpeg"
                onChange={handlePhotos}
                onClick={()=>(setEmptyFile(false))}/>
            </Form.File>
            {emptyFile &&
              <Form.Text style={{color:'red'}}>
                Select Image First
              </Form.Text>
            }
          </Form.Group>
          <Button 
            variant="primary" 
            className='update-button'
            onClick={handleUploadPhotos}>
            {uploading ? "Uploading...":"Upload"}
          </Button>
          {success &&
            <Alert 
              style={{marginTop: "20px"}}  
              onClose={() => setSuccess(false)}  
              variant='success' 
              dismissible>
              Photo Successfully Upload
            </Alert>
          }
        </Card.Body>
      </Card>
    </div>
  )
}
