import React, {useState, useEffect} from 'react'
import {
  Card,
  Button,
  Form,
  Alert} from 'react-bootstrap'
import {getProfile} from '../../mixin'
import {HTTP, updateSchool} from '../../services'

export default function EducationCard() {

  const [school, setSchool] = useState('')
  const [year, setYear] = useState('')
  const [success, setSuccess] = useState(false)

  const handleSchool = e => setSchool(e.target.value)
  const handleYear = e => setYear(e.target.value)

  const handleUpdatSchool =async()=>{
    let graduation = new Date(year)
    let formData = new FormData()
    formData.append("school_name", school);
    formData.append("graduation_time", graduation);
    formData.append("access_token", localStorage.getItem('token'));
    try {
      let response = await HTTP.post(updateSchool, formData)
      console.log(response)
      setSuccess(true)
    } catch (error) {
      console.log(error.response)
    }
  }

  const setSchoolField =(school)=>{
    if(school.school_name ) setSchool(school.school_name)
    if(school.graduation_time) setYear(school.graduation_time)
  }

  useEffect(()=>{
    const loadProfile =async()=>{
      let profileData = await getProfile()
      if(profileData.status){
        if(profileData.status === 200){
          setSchoolField(profileData.data.data.user.education)
        }
      }
    }
    loadProfile()
  },[])

  return (
    <div>
      <Card>
        <Card.Body>
          <Card.Title>Update Last Education</Card.Title>
          <hr/>
          <Form.Group controlId="Shcool">
            <Form.Label>Shcool</Form.Label>
            <Form.Control 
              type="text" 
              placeholder="Shcool" 
              value={school}
              onChange={handleSchool}/>
          </Form.Group>
          <Form.Group controlId="Graduation">
            <Form.Label>Graduation</Form.Label>
            <Form.Control 
              type="date"
              value={year}
              onChange={handleYear}/>
          </Form.Group>
          <Button 
            variant="primary" 
            className='update-button'
            onClick={handleUpdatSchool}>
            Update
          </Button>
          {success &&
            <Alert 
              style={{marginTop: "20px"}}  
              onClose={() => setSuccess(false)}  
              variant='success' 
              dismissible>
              Education Successfully Updated
            </Alert>
          }
        </Card.Body>
      </Card>
    </div>
  )
}
