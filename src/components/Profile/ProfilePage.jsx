import React, { useState, useEffect } from 'react'
import './ProfilePage.scss'
import {Container,
        Row,
        Col,
        Dropdown,
        DropdownButton,
        Button,
        Badge} from 'react-bootstrap'
import avatar from '../../assets/avatar.png'
import {useHistory} from 'react-router-dom'
import {HTTP, revokeOauth} from '../../services'
import {getProfile} from '../../mixin'
import GalleryCard from './GalleryCard'
import EducationCard from './EducationCard'
import JobCard from './JobCard'
import ProfileCard from './ProfileCard'

export default function ProfilePage() {
  let history = useHistory()

  const [profile, setProfile] = useState()
  const [loadProfile, setLoadProfile] = useState(false)

  const goToEdit =()=>{
    history.push('/edit-profile')
  }

  const handleLogout =async()=>{
    let formData = new FormData()
    formData.append("access_token", localStorage.getItem('token'));
    formData.append("confirm", 1);
    try {
      let response = await HTTP.post(revokeOauth,formData)
      console.log(response)
    } catch (error) {
      console.log(error.response)
    }
    // Saya sudah coba pada API revoke selalu Error "Error connecting to Redis on 127.0.0.1:6379 (Errno::ECONNREFUSED)", 
    // tapi access token tetap menjadi invalid
    // jadi disini saya tulis codenya akan selalu logout berhasil atapun error
    localStorage.removeItem('token');
    history.push('/login')
  }

  useEffect(()=>{
    setLoadProfile(true)
    const checkIflogout =()=>{
      if(!localStorage.hasOwnProperty('token')){
        history.push('/login')
      }
    }
    checkIflogout()

    const loadProfile =async()=>{
      let profileData = await getProfile()
      console.log(profileData)
      if(profileData.status){
        if(profileData.status === 200){
          setProfile(profileData.data.data.user)
        }
      }
      
    }
    loadProfile()
    setLoadProfile(false)
  },[history])
  
  return (
    <div className='profile'>
      {!loadProfile && profile &&
        <Container>
          {profile.cover_picture.url ?
            <div className='cover' style = {{ backgroundImage: `url(${profile.cover_picture.url})`}}/> :
            <div className='cover' style = {{ backgroundImage: `url(https://via.placeholder.com/920x190.png?text=Cover+Unset)`}}/>
          }
          <Row>
            <Col sm={3} className='center-avatar'>
              <img src={profile.user_picture ? profile.user_picture.picture.url : avatar} alt="avatar" className="avatar"/>
            </Col>
            <Col sm={9} className='action'>
              <Button className='msg-button'>
                Message <Badge variant="light">0</Badge>
                <span className="sr-only">unread messages</span>
              </Button>
              <DropdownButton id="dropdown-basic-button" title="Setting Account" className='setting-button'>
                <Dropdown.Item onClick={goToEdit}>Edit Profile</Dropdown.Item>
                <Dropdown.Item onClick={handleLogout}>Logout</Dropdown.Item>
              </DropdownButton>
            </Col>
          </Row>
          <Row>
            <Col sm={3} className='profile-data'>
              <ProfileCard profile={profile}/>
            </Col>
            <Col sm={9} className='history-data'>
              <Row>
                <Col sm={6}>
                  <JobCard career={profile.career}/>
                </Col>
                <Col sm={6} className='margin-top'>
                  <EducationCard education={profile.education}/>
                </Col>
              </Row>
              <Row>
                <Col className='gallery-margin'>
                  <GalleryCard user_pictures={profile.user_pictures}/>
                </Col>
              </Row>
            </Col>
          </Row>
        </Container>
      }
    </div>
  )
}
