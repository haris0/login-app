import React from 'react'
import { Card } from 'react-bootstrap'
import {getYears} from '../../mixin'

export default function EducationCard({education}) {
  return (
    <div>
      <Card body>
        <div className='name'>
          Last Education
        </div>
        <div className="hometown">
          {education.school_name ? education.school_name : "School Unset"}
        </div>
        <div className="gender-age">
          {education.graduation_time ? getYears(education.graduation_time) : "Graduation Unset"}
        </div>
      </Card>
    </div>
  )
}
