import React from 'react'
import { Card } from 'react-bootstrap'

export default function GalleryCard({user_pictures}) {
  return (
    <div>
      <Card body >
        <div className='name'>
          Gallery
        </div>
        {user_pictures.length > 0 ?
          <div className="grid-display">
            {user_pictures.map(photo => (
              <div className="photos" key={photo.id}>
                <img src={photo.picture.url} alt='avatar' className='photos-list'/>
              </div>
            ))}
          </div> :
          <div>No Photos</div>
        }
      </Card>
    </div>
  )
}
