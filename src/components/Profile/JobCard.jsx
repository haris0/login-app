import React from 'react'
import { Card } from 'react-bootstrap'
import {getYears} from '../../mixin'

export default function JobCard({career}) {
  return (
    <div>
      <Card body>
        <div className='name'>
          Last Job
        </div>
        <div className="hometown">
          {career.company_name ? career.company_name : "Company Unset"}
        </div>
        <div className="gender-age">
          {career.starting_from ? getYears(career.starting_from) : "Start"} - 
          {career.ending_in ? " "+getYears(career.ending_in)  : " End"}
        </div>
      </Card>
    </div>
  )
}
