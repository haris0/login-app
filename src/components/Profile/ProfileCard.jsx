import React from 'react'
import { Card } from 'react-bootstrap'

export default function ProfileCard({profile}) {
  return (
    <div>
      <Card body>
        <div className='name'>
          {profile.name === profile.id ? "Name Unset" : profile.name}
        </div>
        <div className="hometown">
          {profile.hometown ? profile.hometown : "Hometown Unset"}
        </div>
        <div className="gender-age">
          {profile.gender ? profile.gender : "Gender Unset"} | 
          Age {profile.age ? profile.age : "Unset"}
        </div>
        <hr/>
        <div className="bio">
          {profile.bio ? profile.bio : "Bio Unset"}
        </div>
      </Card>
    </div>
  )
}
