import React, { useState, useMemo } from 'react'
import './RegisterPage.scss'
import {Form,
        Container,
        Card,
        Button,
        Alert} from 'react-bootstrap'
import countryList from 'react-select-country-list'
import {useHistory} from 'react-router-dom'
import {DeviceUUID} from 'device-uuid';
import {HTTP, register} from '../../services'
import {getLatlong, getDeviceType} from '../../mixin'

export default function RegisterPage() {
  
  let history = useHistory()

  const listCountry = useMemo(() => countryList().getLabels(), [])

  const [phone, setPhone] = useState("")
  const [password, setPassword] = useState("")
  const [country, setCountry] = useState(listCountry[0])

  const [errorPhone, setErrorPhone] = useState({status:false, message:""})
  const [errorPass, setErrorPass] = useState({status:false, message:""})
  const [registerLoading, setRegisterLoading] = useState(false)
  const [requesError, setRequesError] = useState({status:false, message:""});

  
  const handleInputPhone = e => setPhone(e.target.value)
  const handleInputPass = e => setPassword(e.target.value)
  const handleInputCountry = e => setCountry(e.target.value)

  const handleInputClick =()=>{
    setErrorPhone({status:false, message:""})
    setErrorPass({status:false, message:""})
  }

  const handleSubmit =async()=>{
    let regNumber = new RegExp('^[0-9]+$')
    if(phone === ""){
      setErrorPhone({status:true, message:"Phone is required"})
    }else if(password === ""){
      setErrorPass({status:true, message:"Password is required"})
    }else if(!regNumber.test(phone)){
      setErrorPhone({status:true, message:"Use Format 62xxx"})
    }else{
      registerRequest()
    }
  }

  const registerRequest =async()=>{
    setRegisterLoading(true)
    let formData = new FormData();
    let latlong = await getLatlong();
    let deviceToken = new DeviceUUID().get();
    let devicetype = getDeviceType()
    formData.append("phone", phone);
    formData.append("password", password);
    formData.append("country", country);
    formData.append("latlong", latlong);
    formData.append("device_token", deviceToken);
    formData.append("device_type", devicetype);
    try {
      let response = await HTTP.post(register, formData)
      console.log(response)
      let data = response.data.data.user
      history.push('/otp/'+data.phone+'/'+data.id)
    } catch (error) {
      console.log(error.response.data.error.errors[0]);
      let errorMsg = error.response.data.error.errors[0]
      setRequesError({status:true, message: errorMsg});
      setPhone('')
      setPassword('')
      setCountry(listCountry[0])
    }
    setRegisterLoading(false)
  }

  return (
    <div className='register'>
      <Container>
        <div className="center-posistion">
          {requesError.status &&
            <Alert  variant='danger' onClose={() => setRequesError({status:false, message:""})} dismissible>
              Error : {requesError.message}
            </Alert>
          }
          <Card body>
            <div className="title">
              Registeration
            </div>
            <Form>
              <Form.Group controlId="formGroupEmail">
                <Form.Label>Phone</Form.Label>
                <Form.Control
                  type="tel"
                  value={phone}
                  onChange={handleInputPhone}
                  onClick={handleInputClick}
                  placeholder="Phone"/>
                {errorPhone.status && 
                  <Form.Text className="text-muted error-text">
                    {errorPhone.message}
                  </Form.Text>
                }
              </Form.Group>
              <Form.Group controlId="formGroupPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type="password"
                  value={password}
                  onChange={handleInputPass}
                  onClick={handleInputClick}
                  placeholder="Password"/>
                {errorPass.status && 
                  <Form.Text className="text-muted error-text">
                    {errorPass.message}
                  </Form.Text>
                }
              </Form.Group>
              <Form.Group controlId="formGroupCountry">
                <Form.Label>Country</Form.Label>
                <Form.Control as="select"
                  value={country}
                  onChange={handleInputCountry}>
                  {listCountry.map(country =>(
                    <option key={country}>{country}</option>
                  ))}
                </Form.Control>
              </Form.Group>
              <span></span>
              <Button variant="primary" onClick={handleSubmit} className='register-button'>
                {registerLoading ? "Loading..." : "Register"}
              </Button>
            </Form>
          </Card>
        </div>
      </Container>
    </div>
  )
}
