import React from 'react'
import './Page404.scss'

export default function Page404() {
  return (
    <div>
      Page Not Found
    </div>
  )
}
