import React, {useState, useEffect} from 'react'
import './LoginPage.scss'
import {Form,
        Container,
        Card,
        Button,
        Alert} from 'react-bootstrap'
import {Link, useHistory} from 'react-router-dom'
import {HTTP, loginOauth} from '../../services'
import {getLatlong, getDeviceType} from '../../mixin'
import {DeviceUUID} from 'device-uuid'

export default function LoginPage() {

  let history = useHistory()

  const [phone, setPhone] = useState("")
  const [password, setPassword] = useState("")

  const [errorPhone, setErrorPhone] = useState({status:false, message:""})
  const [errorPass, setErrorPass] = useState({status:false, message:""})
  const [loginLoading, setLoginLoading] = useState(false)
  const [requesError, setRequesError] = useState({status:false, message:""});

  
  const handleInputPhone = e => setPhone(e.target.value)
  const handleInputPass = e => setPassword(e.target.value)

  const handleInputClick =()=>{
    setErrorPhone({status:false, message:""})
    setErrorPass({status:false, message:""})
  }

  const handleSubmit =()=>{
    let regNumber = new RegExp('^[0-9]+$')
    if(phone === ""){
      setErrorPhone({status:true, message:"Phone is required"})
    }else if(password === ""){
      setErrorPass({status:true, message:"Password is required"})
    }else if(!regNumber.test(phone)){
      setErrorPhone({status:true, message:"Use Format 62xxx"})
    }else{
      oauthLogin()
    }
  }

  const oauthLogin =async()=>{
    setLoginLoading(true)
    let formData = new FormData()
    let latlong = await getLatlong();
    let deviceToken = new DeviceUUID().get();
    let devicetype = getDeviceType()
    formData.append("phone", phone);
    formData.append("password", password);
    formData.append("latlong", latlong);
    formData.append("device_token", deviceToken);
    formData.append("device_type", devicetype);
    try {
      let response = await HTTP.post(loginOauth, formData)
      console.log(response)
      let token = response.data.data.user.access_token
      localStorage.setItem("token", token);
      history.push('/profile')
    } catch (error) {
      console.log(error.response)
      let errorMsg = error.response.data.error.errors[0]
      setRequesError({status:true, message: errorMsg});
      setPhone("")
      setPassword("")
    }
    setLoginLoading(false)
  }

  useEffect(()=>{
    const checkIflogin =()=>{
      if(localStorage.hasOwnProperty('token')){
        history.push('/profile')
      }
    }
    checkIflogin()
  },[history])

  return (
    <div className='login'>
      <Container>
        <div className="center-posistion">
          {requesError.status &&
            <Alert  variant='danger' onClose={() => setRequesError({status:false, message:""})} dismissible>
              Error : {requesError.message}
            </Alert>
          }
          <Card body>
            <div className="title">
              Login
            </div>
            <Form>
              <Form.Group controlId="formGroupEmail">
                <Form.Label>Phone</Form.Label>
                <Form.Control 
                  value={phone}
                  onChange={handleInputPhone}
                  onClick={handleInputClick}
                  type="tel" 
                  placeholder="Phone"/>
                {errorPhone.status && 
                  <Form.Text className="text-muted error-text">
                    {errorPhone.message}
                  </Form.Text>
                }
              </Form.Group>
              <Form.Group controlId="formGroupPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                  value={password} 
                  onChange={handleInputPass}
                  onClick={handleInputClick}
                  type="password" 
                  placeholder="Password"/>
                {errorPass.status && 
                  <Form.Text className="text-muted error-text">
                    {errorPass.message}
                  </Form.Text>
                }
              </Form.Group>
              <span></span>
              <Button variant="primary" onClick={handleSubmit} className='login-button'>
                {loginLoading ? "Loading..." : "Login"}
              </Button>
            </Form>
          </Card>
          <Card body className='register-card'>
            New User? 
            <Link to="/register" className='register-link'>Create New Account</Link>
          </Card>
        </div>
      </Container>
    </div>
  )
}
